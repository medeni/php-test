<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 15.55
 */

namespace App;


use App\Exception\RequestException;
/*
 * to process global variables
 */
class Request
{

    public static function getRequestMethod(){
        if(!isset($_SERVER['REQUEST_METHOD'])){
            throw new RequestException('Request method is not set. Are you running by CLI.');
        }
        return $_SERVER['REQUEST_METHOD'];
    }

}