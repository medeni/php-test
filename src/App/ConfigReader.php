<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 14.29
 */

namespace App;

/**
 * TODO create factory to guess reader by extension
 * Interface ConfigReader
 * @package App
 */
interface ConfigReader
{
    public const CONFIG_DIR_PATH =
        __DIR__ . DIRECTORY_SEPARATOR
        . '..' . DIRECTORY_SEPARATOR
        . '..' . DIRECTORY_SEPARATOR
        . 'config' . DIRECTORY_SEPARATOR;

    /**
     * Reads configuration from fileName relative to CONFIG_DIR_PATH
     * @param string $fileName
     * @return array|null
     */
    public static function read(string $fileName): ?array;
}