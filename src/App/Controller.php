<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 14.24
 */

namespace App;


use App\Exception\ResponseException;

/**
 * Class Controller
 * All actions should be suffixed by Action string
 * @package App
 */
abstract class Controller
{
    private const TEMPLATE_DIR_PATH =
        __DIR__ . DIRECTORY_SEPARATOR
        . '..' . DIRECTORY_SEPARATOR
        . '..' . DIRECTORY_SEPARATOR
        . 'templates' . DIRECTORY_SEPARATOR;

    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {

        $this->request = $request;
    }

    /**
     * @param string $template
     * @param $templateVars
     * @return string
     * @throws ResponseException
     */
    protected function render(string $template, $templateVars): string
    {
        ob_start();
        try {
            extract($templateVars, EXTR_SKIP);
            include_once self::TEMPLATE_DIR_PATH . $template;
        } catch (\Exception $e) {
            throw new ResponseException(sprintf('Unable to process template %s!', $template));
        }
        $output = ob_get_clean();
        if ($output === false) {
            throw new ResponseException('Output buffer is closed or not started');
        }
        return $output;
    }
}