<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 14.22
 */

namespace App;


use App\Exception\RouteNotFoundException;

class Router
{
    /**
     * @var array
     */
    private $routes;
    /** @var array */
    private $route;
    /** @var Controller */
    private $controllerClass;
    /** @var string */
    private $controllerAction;

    /**
     * @var string
     */
    private $uri;

    public function __construct(array $routes, string $uri)
    {
        //TODO Handle invalid configuration
        //TODO Refactor Route to object
        $this->routes = $routes;
        $this->uri = $uri;
    }

    /**
     * @param Request $request
     * @return string
     * @throws RouteNotFoundException
     */
    public function handle(Request $request): string
    {
        $this->route = $this->resolveRoute();
        $this->controllerClass = $this->getControllerClass();
        return (new $this->controllerClass($request))->{$this->controllerAction.'Action'}();
    }

    /**
     * @return string
     * @throws RouteNotFoundException
     */
    private function getControllerClass(): string
    {
        $className = $this->route['controller'] ?? '';
        if(!class_exists($className)){
            throw new RouteNotFoundException(sprintf('Controller %s not found!', $className));
        }
        return $className;
    }

    /**
     * @return array
     * @throws RouteNotFoundException
     */
    private function resolveRoute(): array
    {
        /**
         * @param array $route
         * @return bool
         */
        $setCurrentRoute = function (array $route): bool {
            $paths = array_filter($route['paths'],
                function ($path, $action) {
                    if ($path === $this->uri) {
                        $this->controllerAction = $action;
                        return true;
                    }
                }, ARRAY_FILTER_USE_BOTH);

            return \count($paths);
        };
        /** @var array $matchedRoutes */
        $matchedRoutes = array_filter($this->routes, $setCurrentRoute);
        if (!$this->controllerAction || !\count($matchedRoutes)) {
            throw new RouteNotFoundException(sprintf('Route "%s" not found!', $this->uri));
        }
        //last one found
        return array_pop($matchedRoutes);
    }
}