<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 14.25
 */

namespace App;


use Symfony\Component\Yaml\Yaml;

/**
 * Class YamlConfigReader
 * @package App
 */
class YamlConfigReader implements ConfigReader
{
    private static $configs = [];

    /**
     * @inheritdoc
     */
    public static function read(string $fileName): ?array
    {
        if (array_key_exists($fileName, self::$configs)) {
            return self::$configs[$fileName];
        }

        return self::$configs[$fileName] = Yaml::parseFile(self::CONFIG_DIR_PATH . $fileName);

    }
}