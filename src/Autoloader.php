<?php
/**
 * User: medeni
 * Date: 11/14/2018
 */

//Includes composer auto generated autoload file
if(
    file_exists(
        $vendorAutoloader = __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR .'autoload.php'
    )
)
    include_once $vendorAutoloader;
