<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 14.15
 */

class App
{
    /**
     * @param bool $debug
     */
    public static function boot($debug = true)
    {
        $router = new \App\Router(
            \App\YamlConfigReader::read('routes.yml')['routes'],
            $_SERVER['REQUEST_URI']
        );

        try {
            echo $router->handle(new \App\Request());
        }catch (\Exception $e){//TODO process separate different exceptions
            if($debug) {
                echo $e;
            }
            else{
                echo 'Something went wrong';
            }
        }
        
    }
}