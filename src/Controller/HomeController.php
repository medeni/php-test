<?php
/**
 * Created by PhpStorm.
 * User: medeni
 * Date: 14.10.18.
 * Time: 16.19
 */

namespace Controller;


use App\Controller;


class HomeController extends Controller
{
    /**
     * @return string
     * @throws \App\Exception\ResponseException
     */
    public function indexAction(){
        ///$this->request;

       return $this->render('home.php', ['pageHeader' => 'Welcome home']);
    }
}