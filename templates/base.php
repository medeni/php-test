<!DOCTYPE html>
<html>
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title><?= $pageTitle ?? 'PHP test' ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.css"/>
    <link rel="stylesheet" href="/css/style.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/1.11.8/semantic.min.js"></script>

</head>
<body>
<div class="ui container">
    <div class="ui header aligned centered">
        <?= $pageHeader ?? 'Base Header' ?>
    </div>
    <div class="ui middle aligned center aligned grid">
        <h2 class="column">

            <?= $_mainContent ?? 'Base main Content' ?>
        </h2>
    </div>
</div>
</body>
</html>